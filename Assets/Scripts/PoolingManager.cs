﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * Contains a collection of pools for different objects.
 * GameObjects must implement IPoolable.
 * GameObjects and pools persist between scenes.
 * 
 * Greatly inspired from:
 * https://www.codeproject.com/Articles/188215/Object-Pool-Class-for-C-NET-Applications
 * 
 * IMPORTANT
 * 
 * - If the pools are too small, new objects will be created
 * and will remain there forever, using memory. This implementation
 * tends to grow in memory usage over the course of the game.
 * A way to clean the pools should be added.
 * - If a scene ends and pooled objects are active, they will
 * be destroyed and removed from the pool. Before ending a scene,
 * always try to return the objects to their pools if possible
 * (losing a few shots or explosions is not a big deal, though).
 */
public class PoolingManager {

    private static PoolingManager instance;
    public static PoolingManager Instance {
        get {
            if (instance == null) {
                instance = new PoolingManager();
            }
            return instance;
        }
    }

    private Dictionary<System.Type, ObjectPool> poolContainer;

    public PoolingManager() {
        poolContainer = new Dictionary<System.Type, ObjectPool>();
    }

    public void CreatePool<T>(GameObject prefab, int size) where T : IPoolable {
        if (!poolContainer.ContainsKey(typeof(T))) {
            poolContainer.Add(typeof(T), new ObjectPool(prefab, size));
        }
    }

    public T Spawn<T>() where T : IPoolable {
        if (poolContainer.ContainsKey(typeof(T))) {
            return (T)poolContainer[typeof(T)].SpawnItem();
        } else {
            Debug.LogError("The pool for types " + typeof(T) + " does not exist.");
            return default(T);
        }
    }

    public void Return<T>(T item) where T : IPoolable {
        if (poolContainer.ContainsKey(typeof(T))) {
            poolContainer[typeof(T)].ReturnItem(item);
        } else {
            Debug.LogError("The pool for types " + typeof(T) + " does not exist.");
        }
    }
}