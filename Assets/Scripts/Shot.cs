﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour, IPoolable {

    private float speed = 24f;
    private float lifeTime = 0.5f;

    public void Initialize() {
        gameObject.SetActive(false);
    }

    public void Sleep() {
        gameObject.SetActive(false);
    }

    public void Fire(Vector2 position, Quaternion rotation) {
        gameObject.SetActive(true);
        transform.position = position;
        transform.rotation = rotation;
        StartCoroutine(DelayedReturnToPool());
    }

    public void Update() {
        float maxDistance = Time.deltaTime * speed;
        transform.position = Vector2.MoveTowards(transform.position, transform.right * 100f, maxDistance);
    }

    private IEnumerator DelayedReturnToPool() {
        yield return new WaitForSeconds(lifeTime);
        PoolingManager.Instance.Return(this);
    }

    public GameObject GetGameObject() {
        return gameObject;
    }
}
