﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController instance;

    public Camera mainCamera;
    public GameObject shotPrefab;
    public Transform fireOrigin;

    private Vector2 fireOffset = new Vector2(1f, 0f);
    private int shotCountPerBurst = 50;
    private float fireConeAngleMin = -15f;
    private float fireConeAngleMax = 15f;
    private float burstDuration = 1.5f;
    private float restDuration = 1f;

    public void Awake() {
        instance = this;

        PoolingManager.Instance.CreatePool<Shot>(shotPrefab, 15);
    }

    public void Start() {
        StartCoroutine(FireProcess());
    }

    private IEnumerator FireProcess() {

        float dt, leftOverShotsToFire;
        Shot shot;
        Quaternion shotRotation;

        while (true) {
            float shotsPerSecond = shotCountPerBurst / burstDuration;
            dt = 0f;
            leftOverShotsToFire = 0f;
            while (dt < burstDuration) {
                leftOverShotsToFire += shotsPerSecond * Time.deltaTime;
                for (int i = 0; i < Mathf.FloorToInt(leftOverShotsToFire); i++) {
                    shotRotation = Quaternion.Euler(0f,0f,
                            Random.Range(fireConeAngleMin, fireConeAngleMax));
                    shot = PoolingManager.Instance.Spawn<Shot>();
                    shot.Fire(fireOrigin.position + shotRotation * fireOffset, shotRotation);
                }
                leftOverShotsToFire %= 1;
                dt += Time.deltaTime;
                yield return null;
            }
            yield return new WaitForSeconds(restDuration);
        }
    }
}
