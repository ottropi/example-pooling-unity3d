﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/*
 * Builds a pool of objects using the Unity hierarchy
 * as a stack -> allows to keep objects between scenes
 * by setting the root persistent.
 */
public class ObjectPool {

    private GameObject prefab;
    private Transform persistentPool;

    public ObjectPool(GameObject prefab, int size) {
        this.prefab = prefab;

        // All objects currently in the pool are set
        // as children of the persistentPool, so that they
        // are not destroyed at the end of a scene.
        // When spawned, they are moved to the active scene
        // and become destroyable. This way, they are
        // cleaned if the scene suddenly ends.
        persistentPool = new GameObject().transform;
        GameObject.DontDestroyOnLoad(persistentPool);

        for (int i = 0; i < size; i++) {
            ReturnItem(CreateItem());
        }
    }

    public IPoolable SpawnItem() {
        IPoolable item;

        if (persistentPool.childCount > 0) {
            item = persistentPool.GetChild(0).GetComponent<IPoolable>();
            item.GetGameObject().transform.SetParent(null);
            SceneManager.MoveGameObjectToScene(
                item.GetGameObject(), SceneManager.GetActiveScene());
        } else {
            item = CreateItem();
        }

        return item;
    }

    public void ReturnItem(IPoolable item){
        item.Sleep();
        item.GetGameObject().transform.SetParent(persistentPool);
    }

    private IPoolable CreateItem() {
        IPoolable item = GameObject.Instantiate(prefab).GetComponent<IPoolable>();
        item.Initialize();
        return item;
    }
}