﻿using UnityEngine;
using System.Collections;

public interface IPoolable {

    void Initialize();
    void Sleep();
    GameObject GetGameObject();
}
